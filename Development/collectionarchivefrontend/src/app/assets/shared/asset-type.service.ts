﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { AssetType } from './asset-type.model';

import 'rxjs/add/operator/map';

@Injectable()
export class AssetTypeService {
    private url = 'http://localhost:51990/api/AssetType';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<AssetType> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <AssetType>response.json());
    }

    getList(): Observable<AssetType[]> {
        return this.http.get(this.url)
            .map((response: Response) => <AssetType[]>response.json());
    }

    getDisabled(): Observable<AssetType[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <AssetType[]>response.json());
    }

    create(assetType: AssetType): Observable<AssetType> {
        return this.http.post(this.url, JSON.stringify(assetType), { headers: this.headers })
            .map((response: Response) => <AssetType>response.json());
    }

    edit(assetType: AssetType): Observable<AssetType> {
        return this.http.put(this.url, JSON.stringify(assetType), { headers: this.headers })
            .map((response: Response) => <AssetType>response.json());
    }

    disable(assetType: AssetType): Observable<AssetType> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(assetType) }))
            .map((response: Response) => <AssetType>response.json());
    }

    restore(assetType: AssetType): Observable<AssetType> {
        return this.http.patch(this.url, JSON.stringify(assetType), { headers: this.headers })
            .map((response: Response) => <AssetType>response.json());
    }
}