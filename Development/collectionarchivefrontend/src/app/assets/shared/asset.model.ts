﻿import { CategorizationValue } from '../../categorizations/shared/categorization-value.model';
import { TextFieldValue } from '../../text-fields/shared/text-field-value.model';

export class Asset {
    id: number;
    name: string;
    assetCollectionId: number;
    assetTypeId: number;
    textFieldValues: TextFieldValue[];
    categorizationValues: CategorizationValue[];
}