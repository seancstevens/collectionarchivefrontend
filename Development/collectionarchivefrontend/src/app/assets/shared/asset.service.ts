﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Asset } from './asset.model';

import 'rxjs/add/operator/map';

@Injectable()
export class AssetService {
    private url = 'http://localhost:51990/api/Asset';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<Asset> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <Asset>response.json());
    }

    getList(): Observable<Asset[]> {
        return this.http.get(this.url)
            .map((response: Response) => <Asset[]>response.json());
    }

    getDisabled(): Observable<Asset[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <Asset[]>response.json());
    }

    create(asset: Asset): Observable<Asset> {
        return this.http.post(this.url, JSON.stringify(asset), { headers: this.headers })
            .map((response: Response) => <Asset>response.json());
    }

    edit(asset: Asset): Observable<Asset> {
        return this.http.put(this.url, JSON.stringify(asset), { headers: this.headers })
            .map((response: Response) => <Asset>response.json());
    }

    disable(asset: Asset): Observable<Asset> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(asset) }))
            .map((response: Response) => <Asset>response.json());
    }

    restore(asset: Asset): Observable<Asset> {
        return this.http.patch(this.url, JSON.stringify(asset), { headers: this.headers })
            .map((response: Response) => <Asset>response.json());
    }

    bulkCreate(asset: Asset): Observable<Asset> {
        return this.http.post(this.url + '/BulkPost', JSON.stringify(asset), { headers: this.headers })
            .map((response: Response) => <Asset>response.json());
    }

    bulkEdit(asset: Asset): Observable<Asset> {
        return this.http.put(this.url + '/BulkPut', JSON.stringify(asset), { headers: this.headers })
            .map((response: Response) => <Asset>response.json());
    }

    // BUSINESS LOGIC
    getByCollectionId(id: number): Observable<Asset[]> {
        let params: URLSearchParams = new URLSearchParams(`id=${id.toString()}`);
        return this.http.get(this.url + '/GetByCollectionId', { search: params })
            .map((response: Response) => <Asset[]>response.json());
    }

    getDisabledByCollectionId(id: number): Observable<Asset[]> {
        let params: URLSearchParams = new URLSearchParams(`id=${id.toString()}`);
        return this.http.get(this.url + '/GetDisabledByCollectionId', { search: params })
            .map((response: Response) => <Asset[]>response.json());
    }
}