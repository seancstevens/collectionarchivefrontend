﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from '../users/users.component';
import { UsersDisabledComponent } from '../users/users-disabled.component';
import { CollectionsComponent } from '../collections/collections.component';
import { CollectionsDisabledComponent } from '../collections/collections-disabled.component';
import { CollectionDetailComponent } from '../collections/collection/collection-detail.component';
import { CollectionsDetailDisabledComponent } from '../collections/collection/collection-detail-disabled.component';

const routes: Routes = [
    { path: '', redirectTo: '/users', pathMatch: 'full' },
    { path: 'users', component: UsersComponent },
    { path: 'usersDisabled', component: UsersDisabledComponent },
    { path: 'collections/:id', component: CollectionsComponent },
    { path: 'collectionsDisabled/:id', component: CollectionsDisabledComponent },
    { path: 'collectionDetail/:id', component: CollectionDetailComponent },
    { path: 'collectionDetailDisabled/:id', component: CollectionsDetailDisabledComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }