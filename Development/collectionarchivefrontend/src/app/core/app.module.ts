import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from '../users/users.component';
import { UsersDisabledComponent } from '../users/users-disabled.component';
import { UserService } from '../users/shared/user.service';
import { CollectionsComponent } from '../collections/collections.component';
import { CollectionsDisabledComponent } from '../collections/collections-disabled.component';
import { CollectionDetailComponent } from '../collections/collection/collection-detail.component';
import { CollectionsDetailDisabledComponent } from '../collections/collection/collection-detail-disabled.component';
import { CollectionService } from '../collections/shared/collection.service';
import { AssetService } from '../assets/shared/asset.service';
import { AssetTypeService } from '../assets/shared/asset-type.service';
import { CategorizationService } from '../categorizations/shared/categorization.service';
import { CategorizationOptionService } from '../categorization-options/shared/categorization-option.service';
import { TextFieldService } from '../text-fields/shared/text-field.service';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
    imports: [
        BrowserModule,  
        FormsModule,
        HttpModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        UsersComponent,
        UsersDisabledComponent,
        CollectionsComponent,
        CollectionsDisabledComponent,
        CollectionDetailComponent,
        CollectionsDetailDisabledComponent
    ],
    providers: [
        UserService,
        CollectionService,
        AssetService,
        AssetTypeService,
        CategorizationService,
        CategorizationOptionService,
        TextFieldService
    ],
    bootstrap: [AppComponent]
})  
export class AppModule { }
