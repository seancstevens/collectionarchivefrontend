﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Categorization } from './categorization.model';

import 'rxjs/add/operator/map';

@Injectable()
export class CategorizationService {
    private url = 'http://localhost:51990/api/Categorization';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<Categorization> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <Categorization>response.json());
    }

    getList(): Observable<Categorization[]> {
        return this.http.get(this.url)
            .map((response: Response) => <Categorization[]>response.json());
    }

    getDisabled(): Observable<Categorization[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <Categorization[]>response.json());
    }

    create(categorization: Categorization): Observable<Categorization> {
        return this.http.post(this.url, JSON.stringify(categorization), { headers: this.headers })
            .map((response: Response) => <Categorization>response.json());
    }

    edit(categorization: Categorization): Observable<Categorization> {
        return this.http.put(this.url, JSON.stringify(categorization), { headers: this.headers })
            .map((response: Response) => <Categorization>response.json());
    }

    disable(categorization: Categorization): Observable<Categorization> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(categorization) }))
            .map((response: Response) => <Categorization>response.json());
    }

    restore(categorization: Categorization): Observable<Categorization> {
        return this.http.patch(this.url, JSON.stringify(categorization), { headers: this.headers })
            .map((response: Response) => <Categorization>response.json());
    }

    // BUSINESS LOGIC
    getByCollectionId(id: number) {
        let params: URLSearchParams = new URLSearchParams(`id=${id.toString()}`);
        return this.http.get(this.url + '/GetByCollectionId', { search: params })
            .map((response: Response) => <Categorization[]>response.json());
    }
}