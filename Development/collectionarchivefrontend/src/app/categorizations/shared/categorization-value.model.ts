﻿export class CategorizationValue {
    id: number;
    assetId: number;
    categorizationId: number;
    categorizationOptionId: number;
    inputValue: string;
    
    constructor(id: number) {
        this.categorizationId = id;
    }
}