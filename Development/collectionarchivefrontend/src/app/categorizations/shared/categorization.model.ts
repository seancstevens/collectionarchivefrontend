﻿import { CategorizationOption } from '../../categorization-options/shared/categorization-option.model';

export class Categorization {
    id: number;
    name: string;
    sortOrder: number;
    categorizationOptions: CategorizationOption[];
}