﻿export class CategorizationOption {
    id: number;
    categorizationId: number;
    name: string;
}