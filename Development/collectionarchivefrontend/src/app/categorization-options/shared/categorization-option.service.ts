﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { CategorizationOption } from './categorization-option.model';

import 'rxjs/add/operator/map';

@Injectable()
export class CategorizationOptionService {
    private url = 'http://localhost:51990/api/CategorizationOption';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<CategorizationOption> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <CategorizationOption>response.json());
    }

    getList(): Observable<CategorizationOption[]> {
        return this.http.get(this.url)
            .map((response: Response) => <CategorizationOption[]>response.json());
    }

    getDisabled(): Observable<CategorizationOption[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <CategorizationOption[]>response.json());
    }

    create(categorizationOption: CategorizationOption): Observable<CategorizationOption> {
        return this.http.post(this.url, JSON.stringify(categorizationOption), { headers: this.headers })
            .map((response: Response) => <CategorizationOption>response.json());
    }

    edit(categorizationOption: CategorizationOption): Observable<CategorizationOption> {
        return this.http.put(this.url, JSON.stringify(categorizationOption), { headers: this.headers })
            .map((response: Response) => <CategorizationOption>response.json());
    }

    disable(categorizationOption: CategorizationOption): Observable<CategorizationOption> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(categorizationOption) }))
            .map((response: Response) => <CategorizationOption>response.json());
    }

    restore(categorizationOption: CategorizationOption): Observable<CategorizationOption> {
        return this.http.patch(this.url, JSON.stringify(categorizationOption), { headers: this.headers })
            .map((response: Response) => <CategorizationOption>response.json());
    }
}