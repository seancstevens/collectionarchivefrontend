﻿export class TextFieldValue {
    id: number;
    fieldValue: string;
    assetId: number;
    textFieldId: number;

    constructor(id: number) {
        this.textFieldId = id;
    }
}