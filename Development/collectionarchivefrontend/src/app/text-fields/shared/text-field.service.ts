﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { TextField } from './text-field.model';

import 'rxjs/add/operator/map';

@Injectable()
export class TextFieldService {
    private url = 'http://localhost:51990/api/TextField';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<TextField> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <TextField>response.json());
    }

    getList(): Observable<TextField[]> {
        return this.http.get(this.url)
            .map((response: Response) => <TextField[]>response.json());
    }

    getDisabled(): Observable<TextField[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <TextField[]>response.json());
    }

    create(collection: TextField): Observable<TextField> {
        return this.http.post(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <TextField>response.json());
    }

    edit(collection: TextField): Observable<TextField> {
        return this.http.put(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <TextField>response.json());
    }

    disable(collection: TextField): Observable<TextField> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(collection) }))
            .map((response: Response) => <TextField>response.json());
    }

    restore(collection: TextField): Observable<TextField> {
        return this.http.patch(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <TextField>response.json());
    }

    // BUSINESS LOGIC
    getByCollectionId(id: number) {
        let params: URLSearchParams = new URLSearchParams(`id=${id.toString()}`);
        return this.http.get(this.url + '/GetByCollectionId', { search: params })
            .map((response: Response) => <TextField[]>response.json());
    }

}