import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { User } from './shared/user.model';
import { UserService } from './shared/user.service';

@Component({
    selector: 'my-users',
    moduleId: module.id,
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
    providers: [UserService]
})
export class UsersComponent implements OnInit {
    selectedUser: User;
    newUser: User;
    users: User[];

    constructor(
        private router: Router,
        private userService: UserService) { }

    ngOnInit(): void {
        this.userService.getList()
            .subscribe(users => this.users = users);
    }

    // DISPLAY
    displayNew(): void {
        this.displayCancel();
        this.newUser = new User();
        this.newUser.username = '';
        this.newUser.password = '';
    }

    displayEdit(user: User): void {
        this.displayCancel();
        this.selectedUser = user;
    }

    displayCancel(): void {
        this.newUser = null;
        this.selectedUser = null;
    }

    // CRUD
    create(user: User): void {
        this.userService.create(user)
            .subscribe(user => {
                this.users.push(user);
                this.displayCancel();
            });
    }

    edit(user: User): void {
        this.userService.edit(user)
            .subscribe(user => {
                this.displayCancel();
            });
    }

    disable(user: User): void {
        this.userService.disable(user)
            .subscribe(user => {
                this.removeItem(this.users, user);
                this.displayCancel();
            });
    }
    
    // NAVIGATION
   gotoCollection(user: User): void {
        this.selectedUser = user;
        this.router.navigate(['/collections', this.selectedUser.id]);
    }

   // SHARED
   removeItem(array: any, item: any) {
       let index = -1;
       for (var i = 0; i < array.length; i++) {
           if (array[i].id == item.id) {
               index = i;
           }
       }
       array.splice(index, 1);
   }
}
