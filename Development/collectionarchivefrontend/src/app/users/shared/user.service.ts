﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { User } from './user.model';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    private url = 'http://localhost:51990/api/User';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<User> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <User>response.json());
    }

    getList(): Observable<User[]> {
        return this.http.get(this.url)
            .map((response: Response) => <User[]>response.json());
    }

    getDisabled(): Observable<User[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <User[]>response.json());
    }

    create(user: User): Observable<User> {
        return this.http.post(this.url, JSON.stringify(user), { headers: this.headers })
            .map((response: Response) => <User>response.json());
    }

    edit(user: User): Observable<User> {
        return this.http.put(this.url, JSON.stringify(user), { headers: this.headers })
            .map((response: Response) => <User>response.json());
    }

    disable(user: User): Observable<User> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(user) }))
            .map((response: Response) => <User>response.json());
    }

    restore(user: User): Observable<User> {
        return this.http.patch(this.url, JSON.stringify(user), { headers: this.headers })
            .map((response: Response) => <User>response.json());
    }
}