﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { User } from './shared/user.model';
import { UserService } from './shared/user.service';

@Component({
    selector: 'my-disabled-users',
    moduleId: module.id,
    templateUrl: './users-disabled.component.html',
    styleUrls: ['./users-disabled.component.css'],
    providers: [UserService]
})
export class UsersDisabledComponent implements OnInit {
    users: User[];

    constructor(
        private router: Router,
        private userService: UserService) { }

    ngOnInit(): void {
        this.userService.getDisabled()
            .subscribe(users => this.users = users);
    }

    // CRUD
    restore(user: User): void {
        this.userService.restore(user)
            .subscribe(user => {
                this.removeItem(this.users, user);
            });
    }

    // SHARED
    removeItem(array: any, item: any) {
        let index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id == item.id) {
                index = i;
            }
        }
        array.splice(index, 1);
    }
}
