﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Collection } from './collection.model';

import 'rxjs/add/operator/map';

@Injectable()
export class CollectionService {
    private url = 'http://localhost:51990/api/AssetCollection';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    // CRUD
    get(id: number): Observable<Collection> {
        return this.http.get(this.url + '/' + id)
            .map((response: Response) => <Collection>response.json());
    }

    getList(): Observable<Collection[]> {
        return this.http.get(this.url)
            .map((response: Response) => <Collection[]>response.json());
    }

    getDisabled(): Observable<Collection[]> {
        return this.http.get(this.url + '/GetDisabled')
            .map((response: Response) => <Collection[]>response.json());
    }

    create(collection: Collection): Observable<Collection> {
        return this.http.post(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <Collection>response.json());
    }

    edit(collection: Collection): Observable<Collection> {
        return this.http.put(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <Collection>response.json());
    }

    disable(collection: Collection): Observable<Collection> {
        return this.http.delete(this.url, new RequestOptions({ headers: this.headers, body: JSON.stringify(collection) }))
            .map((response: Response) => <Collection>response.json());
    }

    restore(collection: Collection): Observable<Collection> {
        return this.http.patch(this.url, JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <Collection>response.json());
    }

    bulkCreate(collection: Collection): Observable<Collection> {
        return this.http.post(this.url + '/BulkPost', JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <Collection>response.json());
    }

    bulkEdit(collection: Collection): Observable<Collection> {
        return this.http.put(this.url + '/BulkPut', JSON.stringify(collection), { headers: this.headers })
            .map((response: Response) => <Collection>response.json());
    }

    // BUSINESS LOGIC
    getByUserId(userId: number): Observable<Collection[]> {
        let params: URLSearchParams = new URLSearchParams(`userId=${userId.toString()}`);
        return this.http.get(this.url + '/GetByUserId', { search: params })
            .map((response: Response) => <Collection[]>response.json());
    }

    getDisabledByUserId(userId: number): Observable<Collection[]> {
        let params: URLSearchParams = new URLSearchParams(`userId=${userId.toString()}`);
        return this.http.get(this.url + '/GetDisabledByUserId', { search: params })
            .map((response: Response) => <Collection[]>response.json());
    }

    getCollectionDetail(id: number): Observable<Collection> {
        let params: URLSearchParams = new URLSearchParams(`id=${id.toString()}`);
        return this.http.get(this.url + '/GetCollectionDetail', { search: params })
            .map((response: Response) => <Collection>response.json());
    }
}