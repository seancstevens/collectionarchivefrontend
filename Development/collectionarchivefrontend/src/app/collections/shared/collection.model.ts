﻿import { Categorization } from '../../categorizations/shared/categorization.model';
import { TextField } from '../../text-fields/shared/text-field.model';

export class Collection {
    id: number;
    name: string;
    sortOrder: number;
    userId: number;
    categorizations: Categorization[];
    textFields: TextField[];
}