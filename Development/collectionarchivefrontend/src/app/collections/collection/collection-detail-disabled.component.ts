﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Collection } from '../shared/collection.model';
import { CollectionService } from '../shared/collection.service';
import { Asset } from '../../assets/shared/asset.model';
import { AssetService } from '../../assets/shared/asset.service';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'my-collection-detail-disabled',
    moduleId: module.id,
    templateUrl: './collection-detail-disabled.component.html',
    styleUrls: ['./collection-detail-disabled.component.css'],
    providers: [CollectionService, AssetService]
})
export class CollectionsDetailDisabledComponent implements OnInit {
    @Input()
    selectedCollection: Collection;
    assets: Asset[];

    constructor(
        private collectionService: CollectionService,
        private assetService: AssetService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.collectionService.getCollectionDetail(+params['id']))
            .subscribe(collection => this.selectedCollection = collection);
        this.route.params
            .switchMap((params: Params) => this.assetService.getDisabledByCollectionId(+params['id']))
            .subscribe(asset => this.assets = asset);
    }

    // CRUD
    restore(asset: Asset): void {
        this.assetService.restore(asset)
            .subscribe(asset => {
                this.removeItem(this.assets, asset);
            });
    }

    // NAVIGATION
    goBack(): void {
        this.location.back();
    }

    // SHARED
    removeItem(array: any, item: any) {
        let index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id == item.id) {
                index = i;
            }
        }
        array.splice(index, 1);
    }
}