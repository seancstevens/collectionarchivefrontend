﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Collection } from '../shared/collection.model';
import { CollectionService } from '../shared/collection.service';
import { Categorization } from '../../categorizations/shared/categorization.model';
import { CategorizationService } from '../../categorizations/shared/categorization.service';
import { CategorizationOption } from '../../categorization-options/shared/categorization-option.model';
import { CategorizationOptionService } from '../../categorization-options/shared/categorization-option.service';
import { CategorizationValue } from '../../categorizations/shared/categorization-value.model';
import { TextField } from '../../text-fields/shared/text-field.model';
import { TextFieldService } from '../../text-fields/shared/text-field.service';
import { TextFieldValue } from '../../text-fields/shared/text-field-value.model';
import { Asset } from '../../assets/shared/asset.model';
import { AssetService } from '../../assets/shared/asset.service';
import { AssetType } from '../../assets/shared/asset-type.model';
import { AssetTypeService } from '../../assets/shared/asset-type.service';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'my-collection-detail',
    moduleId: module.id,
    templateUrl: './collection-detail.component.html',
    styleUrls: ['./collection-detail.component.css'],
    providers: [CollectionService, CategorizationService, TextFieldService]
})
export class CollectionDetailComponent implements OnInit {
    @Input()
    selectedCollection: Collection;
    assets: Asset[];
    newAsset: Asset;
    editAsset: Asset;
    selectedAsset: Asset;
    assetTypes: AssetType[];
    categorizations: Categorization[];
    textFields: TextField[];

    constructor(
        private collectionService: CollectionService,
        private assetService: AssetService,
        private assetTypeService: AssetTypeService,
        private categorizationService: CategorizationService,
        private categorizationOptionService: CategorizationOptionService,
        private textFieldService: TextFieldService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.collectionService.getCollectionDetail(+params['id']))
            .subscribe(collection => this.selectedCollection = collection);
        this.route.params
            .switchMap((params: Params) => this.assetService.getByCollectionId(+params['id']))
            .subscribe(assets => {
                this.assets = assets;
                this.sortAssetsArray();
            });
        this.route.params
            .switchMap((params: Params) => this.assetTypeService.getList())
            .subscribe(assetTypes => this.assetTypes = assetTypes);
        this.route.params
            .switchMap((params: Params) => this.categorizationService.getByCollectionId(+params['id']))
            .subscribe(categorizations => this.categorizations = categorizations);
        this.route.params
            .switchMap((params: Params) => this.textFieldService.getByCollectionId(+params['id']))
            .subscribe(textFields => this.textFields = textFields);
    }

    // DISPLAY
    displayNew(): void {
        this.displayCancel();
        this.newAsset = new Asset();
        this.newAsset.name = '';
        this.newAsset.assetCollectionId = this.selectedCollection.id;
        this.newAsset.categorizationValues = new Array<CategorizationValue>();
        for (var i = 0; i < this.categorizations.length; i++) {
            this.newAsset.categorizationValues[i] = new CategorizationValue(this.categorizations[i].id);
        }
        this.newAsset.textFieldValues = new Array<TextFieldValue>();
        for (var i = 0; i < this.textFields.length; i++) {
            this.newAsset.textFieldValues[i] = new TextFieldValue(this.textFields[i].id);
        }
    }

    displayEdit(asset: Asset): void {
        this.displayCancel();
        this.editAsset = asset;
        for (var i = 0; i < this.categorizations.length; i++) {
            if (!this.editAsset.categorizationValues)
                this.editAsset.categorizationValues = new Array<CategorizationValue>();
            if (this.editAsset.categorizationValues.filter(x => x.categorizationId === this.categorizations[i].id).length == 0)
                this.editAsset.categorizationValues.push(new CategorizationValue(this.categorizations[i].id));
        }
        for (var i = 0; i < this.textFields.length; i++) {
            if (!this.editAsset.textFieldValues)
                this.editAsset.textFieldValues = new Array<TextFieldValue>();
            if (this.editAsset.textFieldValues.filter(x => x.textFieldId === this.textFields[i].id).length == 0)
                this.editAsset.textFieldValues.push(new TextFieldValue(this.textFields[i].id));
        }
    }

    displayDetail(asset: Asset): void {
        this.displayCancel();
        this.selectedAsset = asset;
    }

    displayCancel(): void {
        this.newAsset = null;
        this.editAsset = null;
        this.selectedAsset = null;
        this.route.params
            .switchMap((params: Params) => this.assetService.getByCollectionId(+params['id']))
            .subscribe(assets => {
                this.assets = assets;
                this.sortAssetsArray();
            });
    }

    // CRUD
    create(asset: Asset): void {
        this.assetService.create(asset)
            .subscribe(asset => {
                this.assets.push(asset);
                this.displayCancel();
            });
    }

    edit(asset: Asset): void {
        this.assetService.edit(asset)
            .subscribe(asset => {
                this.displayCancel();
            });
    }

    disable(asset: Asset): void {
        this.assetService.disable(asset)
            .subscribe(asset => {
                this.removeItem(this.assets, asset);
                this.displayCancel();
            });
    }

    createCategorizationOption(categorizationOption: CategorizationOption): void {
        this.categorizationOptionService.create(categorizationOption)
            .subscribe(categorizationOption => {
                let categorization = this.categorizations.filter(x => x.id === categorizationOption.categorizationId)
                categorization[0].categorizationOptions.push(categorizationOption);
            });
    }

    bulkCreate(asset: Asset): void {
        this.assetService.bulkCreate(asset)
            .subscribe(asset => {
                this.assets.push(asset);
                this.displayCancel();
            });
    }

    bulkEdit(asset: Asset): void {
        this.assetService.bulkEdit(asset)
            .subscribe(asset => {
                this.displayCancel();
            });
    }

    // NAVIGATION
    goBack(): void {
        this.location.back();
    }

    gotoAssetsDisabled(): void {
        this.router.navigate(['/collectionDetailDisabled', this.selectedCollection.id]);
    }

    // SHARED

    removeItem(array: any, item: any) {
        let index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id == item.id) {
                index = i;
            }
        }
        array.splice(index, 1);
    }

    sortAssetsArray() {
        this.assets.sort((a, b) => {
            if (a.name < b.name) return -1;
            else if (a.name > b.name) return 1;
            else return 0;
        });
    }

    getAssetTypeName(id: number): string {
        let assetType = this.assetTypes.filter(x => x.id === id);
        return assetType[0].name;
    }

    getTextFieldName(id: number): string {
        let textField = this.textFields.filter(x => x.id === id);
        return textField[0].name;
    }

    getCategorizationName(id: number): string {
        let categorization = this.categorizations.filter(x => x.id === id);
        return categorization[0].name;
    }

    getCategorizationIndex(id: number): number {
        let categorization = this.categorizations.filter(x => x.id === id);
        return this.categorizations.indexOf(categorization[0]);
    }

    getCategorizationOptionName(categorizationId: number, categorizationOptionId: number): string {
        let categorization = this.categorizations.filter(x => x.id === categorizationId);
        let categorizationOption = categorization[0].categorizationOptions.filter(x => x.id === categorizationOptionId);
        return categorizationOption[0].name;
    }

    addNewCategorizationOption(categorizationValue: CategorizationValue) {
        let test = CategorizationValue;
        let categorizationOption = new CategorizationOption();
        categorizationOption.categorizationId = categorizationValue.categorizationId;
        categorizationOption.name = categorizationValue.inputValue;
        this.createCategorizationOption(categorizationOption);
        categorizationValue.inputValue = '';
    }
}