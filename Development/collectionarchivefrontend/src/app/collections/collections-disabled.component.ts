﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { User } from '../users/shared/user.model';
import { UserService } from '../users/shared/user.service';
import { Collection } from './shared/collection.model';
import { CollectionService } from './shared/collection.service';

@Component({
    selector: 'my-disabled-collections',
    moduleId: module.id,
    templateUrl: './collections-disabled.component.html',
    styleUrls: ['./collections-disabled.component.css'],
    providers: [UserService, CollectionService]
})
export class CollectionsDisabledComponent implements OnInit {
    selectedUser: User;
    collections: Collection[];

    constructor(
        private userService: UserService,
        private collectionService: CollectionService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.userService.get(+params['id']))
            .subscribe(user => this.selectedUser = user);
        this.route.params
            .switchMap((params: Params) => this.collectionService.getDisabledByUserId(+params['id']))
            .subscribe(collection => this.collections = collection);
    }

    // CRUD
    restore(collection: Collection): void {
        this.collectionService.restore(collection)
            .subscribe(collection => {
                this.removeItem(this.collections, collection);
            });
    }

    // NAVIGATION

    goBack(): void {
        this.location.back();
    }

    // SHARED
    removeItem(array: any, item: any) {
        let index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id == item.id) {
                index = i;
            }
        }
        array.splice(index, 1);
    }
}
