﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../users/shared/user.model';
import { UserService } from '../users/shared/user.service';
import { Collection } from './shared/collection.model';
import { CollectionService } from './shared/collection.service';
import { Categorization } from '../categorizations/shared/categorization.model';
import { TextField } from '../text-fields/shared/text-field.model';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'my-collections',
    moduleId: module.id,
    templateUrl: './collections.component.html',
    styleUrls: ['./collections.component.css'],
    providers: [UserService, CollectionService]
})
export class CollectionsComponent implements OnInit {
    @Input()
    selectedUser: User;
    editCollection: Collection;
    newCollection: Collection;
    collections: Collection[];

    constructor(
        private userService: UserService,
        private collectionService: CollectionService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.userService.get(+params['id']))
            .subscribe(user => this.selectedUser = user);
        this.route.params
            .switchMap((params: Params) => this.collectionService.getByUserId(+params['id']))
            .subscribe(collection => this.collections = collection);
    }

    // DISPLAY
    displayNew(): void {
        this.displayCancel();
        this.newCollection = new Collection();
        this.newCollection.name = '';
        this.newCollection.userId = this.selectedUser.id;
        this.newCollection.categorizations = new Array<Categorization>();
        this.newCollection.textFields = new Array<TextField>();
    }

    displayEdit(collection: Collection): void {
        this.displayCancel();
        this.editCollection = collection;
    }

    displayCancel(): void {
        this.newCollection = null;
        this.editCollection = null;
    }

    // CRUD
    create(collection: Collection): void {
        this.collectionService.create(collection)
            .subscribe(collection => {
                this.collections.push(collection);
                this.displayCancel();
            });
    }

    edit(collection: Collection): void {
        this.collectionService.edit(collection)
            .subscribe(collection => {
                this.displayCancel();
            });
    }

    disable(collection: Collection): void {
        this.collectionService.disable(collection)
            .subscribe(collection => {
                this.removeItem(this.collections, collection);
                this.displayCancel();
            });
    }

    bulkCreate(collection: Collection): void {
        this.collectionService.bulkCreate(collection)
            .subscribe(collection => {
                this.collections.push(collection);
                this.displayCancel();
            });
    }

    bulkEdit(collection: Collection): void {
        this.collectionService.bulkEdit(collection)
            .subscribe(collection => {
                this.displayCancel();
            });
    }

    // NAVIGATION
    gotoCollectionDetail(collection: Collection): void {
        this.router.navigate(['/collectionDetail', collection.id]);
    }

    gotoCollectionsDisabled(): void {
        this.router.navigate(['/collectionsDisabled', this.selectedUser.id]);
    }

    goBack(): void {
        this.location.back();
    }

    // SHARED
    removeItem(array: any, item: any) {
        let index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id == item.id) {
                index = i;
            }
        }
        array.splice(index, 1);
    }

    addCategorization(collection: Collection): void {
        collection.categorizations.push(new Categorization());
    }

    addTextField(collection: Collection): void {
        collection.textFields.push(new TextField());
    }
}