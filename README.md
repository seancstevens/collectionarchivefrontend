# README #

### What is this repository for? ###

* Collection Archive - Front End
* Built using Angular (Angular 2)
* Version 1
* https://seancstevens@bitbucket.org/seancstevens/collectionarchivefrontend.git

### How do I get set up? ###

* Make sure back end project is installed and running.
* Navigate to directory containing collectionarchivefrontend.csproj in a command prompt.
* Execute command 'npm start'.

### Contribution guidelines ###

* Any code review and feedback would be appreciated.

### Who do I talk to? ###

* Sean Stevens
* sean.stevens@stgconsulting.com